package org.mine.ezapp.messenger.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.mine.ezapp.messenger.database.DatabaseClass;
import org.mine.ezapp.messenger.entity.Profile;

public class ProfileService {

	private Map<String, Profile> profiles = DatabaseClass.getProfiles();
	
	public ProfileService() {
		profiles.put("kirill", new Profile(1L,"kirill petrov", "kirill", "petrov"));
	}
	
	public List<Profile> getAllProfiles() {
		return new ArrayList<Profile>(profiles.values());
	}
	
	public Profile getProfile(String profileName) {
		return profiles.get(profileName);
	}
	
	public Profile addProfile(Profile profile) {
		profile.setId(profiles.size() + 1);
		profiles.put(profile.getProfileName(), profile);
		return profile;
	}
	
	public Profile updateProfile(Profile profile) {
		if (profile.getProfileName().isEmpty()) {
			return null;
		}
		return profiles.put(profile.getProfileName(), profile);
	}
	
	public Profile removeProfile(String profileName) {
		return profiles.remove(profileName);
	}
}
