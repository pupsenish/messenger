package org.mine.ezapp.messenger.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.mine.ezapp.messenger.database.DatabaseClass;
import org.mine.ezapp.messenger.entity.Message;
import org.mine.ezapp.messenger.exception.DataNotFoundException;

public class MessageService {

	private Map<Long, Message> messages = DatabaseClass.getMessages();

	public MessageService() {
		messages.put(1L, new Message(1L, "Hello my name is Vasya", "vasya"));
		messages.put(2L, new Message(2L, "Hi Im Tanya", "tanya"));
		messages.put(3L, new Message(3L, "toodo toodoto to", "Mysterious Stranger"));
	}
	
	public List<Message> getAllMessages() {
		return new ArrayList<Message>(messages.values());
	}
	
	public List<Message> getAllMessagesForYear(int year) {
		List<Message> messagesForYear = new ArrayList<>();
		Calendar calendar = Calendar.getInstance();
		for (Message message : messages.values()) {
			calendar.setTime(message.getCreated());
			if (calendar.get(Calendar.YEAR) == year) {
				messagesForYear.add(message);
			}
		}
		return messagesForYear;
	}
	
	public List<Message> getAllMessagesPaginated(int start, int size) {
		List<Message> list = new ArrayList<>(messages.values());
		if (start + size > list.size()) {
			return new ArrayList<>();
		}
		return list.subList(start, start + size);
		
	}
	
	public Message getMessage(long id) {
		Message msg = messages.get(id);
		if (msg == null) {
			throw new DataNotFoundException("message not found");
		}
		return msg;
	}
	
	public Message addMessage(Message msg) {
		msg.setId(messages.size() + 1);
		messages.put(msg.getId(), msg);
		return msg;
	}
	
	public Message updateMessage(Message msg) {
		if (msg.getId() <= 0) {
			return null;
		}
		messages.put(msg.getId(), msg);
		return msg;
	}
	
	public Message removeMessage(long id) {
		return messages.remove(id);
	}
}
