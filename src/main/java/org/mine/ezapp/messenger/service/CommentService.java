package org.mine.ezapp.messenger.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.mine.ezapp.messenger.database.DatabaseClass;
import org.mine.ezapp.messenger.entity.Comment;
import org.mine.ezapp.messenger.entity.ErrorMessage;
import org.mine.ezapp.messenger.entity.Message;

public class CommentService {

	private Map<Long, Message> messages = DatabaseClass.getMessages();
	private Map<Long, Comment> comments = DatabaseClass.getComments();
	
	public CommentService() {
		comments.put(1L, new Comment(1L, "get back to work", "kp", new Date()));
		comments.put(2L, new Comment(2L, "get back to work", "kp", new Date()));
		comments.put(3L, new Comment(3L, "get back to work", "kp", new Date()));
		messages.get(3L).setComments(comments);
	}
	
	public List<Comment> getAllComments(long messageId) {
		 Map<Long, Comment> comments = messages.get(messageId).getComments();
		 return new ArrayList<>(comments.values());
	}
	
	public Comment getComment(long messageId, long commentId) {
		Message msg = messages.get(messageId);
		ErrorMessage errorMessage = new ErrorMessage("Not found!", 404, "www.allgone.org");
		Response response = Response.status(Status.NOT_FOUND).entity(errorMessage).build();
		if (msg == null) {
			throw new WebApplicationException(response);
		}
		Map<Long, Comment> comments = messages.get(messageId).getComments();
		Comment comment = comments.get(commentId);
		if (comment == null) {
			throw new NotFoundException(response);
		}
		return comment;
	}
	
	public Comment addComment(long messageId, Comment comment) {
		Map<Long, Comment> comments = messages.get(messageId).getComments();
		comment.setId(comments.size() + 1);
		return comments.put(comment.getId(), comment);
	}
	
	public Comment updateComment(long messageId, Comment comment) {
		Map<Long, Comment> comments = messages.get(messageId).getComments();
		if (comment.getId() <= 0) {
			return null;
		}
		return comments.put(comment.getId(), comment);
	}
	
	public Comment removeComment(long messageId, long commentId) {
		Map<Long, Comment> comments = messages.get(messageId).getComments();
		return comments.remove(commentId);
	}
}
