package org.mine.ezapp.messenger.exception;

public class DataNotFoundException extends RuntimeException {

	private static final long SERIAL_ID = -213412324L;
	
	public DataNotFoundException(String message) {
		super(message);
	}
}
